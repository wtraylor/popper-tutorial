# SPDX-FileCopyrightText: 2020 Wolfgang Traylor <wolfgang.traylor@senckenberg.de>
#
# SPDX-License-Identifier: CC0-1.0

Popper-Tutorial-Series.pdf: tutorial_1.md tutorial_2.md tutorial_3.md pandoc/listings-setup.tex pandoc/metadata.yml
	pandoc -o $@ \
		--pdf-engine xelatex \
		--listings -H pandoc/listings-setup.tex \
		--metadata-file pandoc/metadata.yml \
		tutorial_1.md tutorial_2.md tutorial_3.md
