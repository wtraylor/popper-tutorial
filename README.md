<!--
SPDX-FileCopyrightText: 2020 Wolfgang Traylor <wolfgang.traylor@posteo.de>

SPDX-License-Identifier: CC-BY-4.0
-->

[![REUSE-compliant](figures/reuse-compliant.svg)](https://reuse.software)

# Popper Tutorial

Author: Wolfgang Traylor, `wolfgang.traylor@senckenberg.de`

All tutorials of this series are stored in one repository.
The stage of the example project is available in the respective branches `tutorial_1`, `tutorial_2`, etc.

## Creating a PDF
You can run `make` to create a PDF with all tutorials in one document.
This was tested with Pandoc 2.11.1 and `XeTeX 3.14159265-2.6-0.999992 (TeX Live 2020/Arch Linux)`.

## License
This tutorial series is licensed under the Creative Commons Attribution 4.0 International License.
To view a copy of this license, visit <http://creativecommons.org/licenses/by/4.0/> or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
